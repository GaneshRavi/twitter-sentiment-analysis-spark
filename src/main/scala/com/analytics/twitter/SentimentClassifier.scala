package com.analytics.twitter

import org.apache.spark.streaming.{ Seconds, StreamingContext }
import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import java.io.{ PrintWriter, File }
import scala.io.Source
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.feature.HashingTF
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.classification.NaiveBayes
import twitter4j.conf.ConfigurationBuilder
import com.analytics.stream.receivers.TweetStreamReceiver

object SentimentClassifier {

  final val StopWordsFilename = "stopwords_eng.txt"
  final val SlidingWindowSizeSecs = 60
  final val SlideLengthSecs = 60
  final val TrainingDataFilename = "sentiment_train.csv"
  final val OutputDirectory = System.getProperty("user.home") + File.separator + "classified_tweets"

  def main(args: Array[String]) {

    if (args.length < 4) {
      System.err.println("Usage:")
      System.err.println("TopTwitterWords <consumer key> <consumer secret> " +
        "<access token> <access token secret> <filters>")
      System.exit(1)
    }

    new File(OutputDirectory).mkdirs
    var fileNameCounter = 1
    val stopWords = Source.fromInputStream(getClass.getResourceAsStream(StopWordsFilename)).getLines.toSet

    val Array(consumerKey, consumerSecret, accessToken, accessTokenSecret) = args.take(4)
    val filters: Array[String] = args.takeRight(args.length - 4)

    val sparkConf = new SparkConf().setAppName("SentimentClassifier")
    val sc = new SparkContext(sparkConf)

    val htf = new HashingTF(10000)
    val trainingDataLines = Source.fromInputStream(getClass.getResourceAsStream(TrainingDataFilename)).getLines.toList
    val trainingDataLinesRdd = sc.parallelize(trainingDataLines)
    val trainingData = trainingDataLinesRdd.map(text =>
      // 0 label -> Negative, 1 label -> Positive
      if (text.charAt(0) == '0') {
        LabeledPoint(0, htf.transform(text.toLowerCase.split("(?=\\.)|\\s+")));
      } else {
        LabeledPoint(1, htf.transform(text.toLowerCase.split("(?=\\.)|\\s+")))
      })
    val model = NaiveBayes.train(trainingData, 1.0, "multinomial")

    val ssc = new StreamingContext(sc, Seconds(2))
    val stream = ssc.receiverStream(new TweetStreamReceiver(consumerKey, consumerSecret, accessToken, accessTokenSecret, filters))
    val tweets = stream.map(_.getText)
    val tweetsCount = tweets.count()
    val tweetsClassified = tweets.map((text: String) =>
      (model.predict(htf.transform(text.toLowerCase.split("(?=\\.)|\\s+"))), 1))
      .reduceByKeyAndWindow((a: Int, b: Int) => (a + b), Seconds(SlidingWindowSizeSecs), Seconds(SlideLengthSecs))
      .map { case (sentiment, count) => (count, sentiment) }
      .transform(_.sortByKey(false))

    tweetsClassified.foreachRDD(rdd => {
      val sentimentsList = rdd.collect()
      println("Sentiments in last 60 seconds:")
      rdd.foreach { case (count, sentiment) => println("%s (%d tweets)".format(sentiment, count)) }
      val writer = new PrintWriter(new File(OutputDirectory + File.separator + fileNameCounter + ".txt"))
      sentimentsList.foreach { case (count, sentiment) => writer.println(sentiment + ", " + count + " tweets") }
      fileNameCounter = fileNameCounter + 1
      writer.close()
    })

    ssc.start()
    ssc.awaitTermination()
  }
}
